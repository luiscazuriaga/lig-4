// container principal onde vai o jogo todo
let mainContainer = document.getElementById('mainContainer')
//Willian
/*
 * Cria uma matriz n x n preenchida por zeros
 */
let grade = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0]
];

function criarGrade() {
    let caixaContainer = document.createElement("div");
    caixaContainer.id = "caixaContainer"
    mainContainer.appendChild(caixaContainer)

    grade.map((i) => i.map((j) => {
        const imgContainner = document.createElement('div')
        imgContainner.className = 'imgContainner'

        let criarCaixa = document.createElement("div");
        criarCaixa.className = "caixa";
        imgContainner.appendChild(criarCaixa)
        caixaContainer.appendChild(imgContainner);

        if (j == 1) {
            criarCaixa.className = "caixa caixa1";
        } else if (j == 2) {
            criarCaixa.className = "caixa caixa2";
        }
    })
    );
}
criarGrade();

function updateGrade() {
    mainContainer.removeChild(caixaContainer);
    criarGrade();
}

// Luis

/* 
Cria os botões que serão clicados para escolha da coluna
*/
let buttonsBox = document.createElement('div')
buttonsBox.id = "buttonsBox"
mainContainer.appendChild(buttonsBox)

const createButtons = () => {
    for (let a = 0; a <= 6; a++) {
        let columnButtons = document.createElement('columnButtons')
        columnButtons.id = `${a}`
        columnButtons.className = "columnButtons"
        buttonsBox.appendChild(columnButtons)
    }
}
createButtons()

// Click Handler
// Variaveis globais 
let player = 1
const columnButtons = document.getElementsByClassName('columnButtons')
const winnerDiv = document.getElementById('winner')
let selectedColumnButton = columnButtons[0]
function showPlayer() {

}
const playerTurn = document.createElement('div')
const playerDisk = document.createElement('img')
playerTurn.id = 'playerTurn'
playerDisk.id = 'playerImg'
playerTurn.appendChild(playerDisk)
mainContainer.appendChild(playerTurn)
playerDisk.src = 'src/turnPlayer1.png'



let rigthLegs = document.getElementById('rigthLegs')
let leftLegs = document.getElementById('leftLegs')


// Teste playerChange
function playerchange() {

    if (player === 1) {
        recognizerHead.style.backgroundImage = "url('./src/recognizerHead2.png')"
        rigthLegs.style.backgroundImage = "url('./src/rightLeg2.png')"
        leftLegs.style.backgroundImage = "url('./src/leftLeg2.png')"
        playerDisk.src = 'src/turnPlayer2.png'
        player = 2
    }
    else if (player === 2) {
        recognizerHead.style.backgroundImage = "url('./src/recognizerHead1.png')"
        rigthLegs.style.backgroundImage = "url('./src/rightLeg1.png')"
        leftLegs.style.backgroundImage = "url('./src/leftLeg1.png')"
        playerDisk.src = 'src/turnPlayer1.png'
        player = 1
    }
}

// Muda o valor do ultimo 0 da coluna
const changeValueArray = () => {
    selectedColumnButton = event.target;
    for (let aux = 5; aux >= 0; aux--) {// são 7 botões
        if (grade[aux][selectedColumnButton.id] === 0) {
            grade[aux][selectedColumnButton.id] = player;
            updateGrade();
            return true;
        }
    }
    return false;
}

// Verifica empate
function drawVerify() {
    const drawImage = document.createElement('img')
    drawImage.src = 'src/draw.png'
    for (let aux = 0; aux < grade[0].length; aux++) {
        if (grade[0].indexOf(0) === -1) {
            const drawDiv = document.getElementById('draw')
            drawDiv.appendChild(drawImage)
            drawDiv.style.display = 'flex'
            return true
        }
        else {

            return false
        }
    }
}
const playerWinner = document.createElement('img')
// Verificar se o jogador venceu
function winnerVerify() {

    playerWinner.src = 'src/winnerPlayer' + player + '.png'
    if (drawVerify() === false) {
        for (let y = 0; y < grade.length; y++) {
            for (let x = 0; x < grade[0].length; x++) {
                let disk = grade[y][x];
                if (disk !== 0) {
                    // Horizontalmente ----
                    if (disk === grade[y][x + 1] && disk === grade[y][x + 2] && disk === grade[y][x + 3]) {
                        winnerDiv.appendChild(playerWinner)
                        winnerDiv.style.display = 'flex'
                        stopTimer()
                        removeEventButtonClick()
                    }
                    if (y - 3 >= 0) {
                        // Verticalmente |
                        if (disk === grade[y - 1][x] && disk === grade[y - 2][x] && disk === grade[y - 3][x]) {
                            winnerDiv.appendChild(playerWinner)
                            winnerDiv.style.display = 'flex'
                            stopTimer()
                            removeEventButtonClick()
                        }
                        // Diagonalmente /
                        else if (disk === grade[y - 1][x + 1] && disk === grade[y - 2][x + 2] && disk === grade[y - 3][x + 3]) {
                            winnerDiv.appendChild(playerWinner)
                            winnerDiv.style.display = 'flex'
                            stopTimer()
                            removeEventButtonClick()
                        }
                        // Diagonalmente inverso \ 
                        if (x - 3 >= 0) {
                            if (disk === grade[y - 1][x - 1] && disk === grade[y - 2][x - 2] && disk === grade[y - 3][x - 3]) {
                                winnerDiv.appendChild(playerWinner)
                                winnerDiv.style.display = 'flex'
                                stopTimer()
                                removeEventButtonClick()
                            }
                        }

                    }

                }
            }
        }
    }

}

// Verifica se a coluna foi selecionada  
const eventColumnSelect = function (event) {
    if (changeValueArray(event) === false) {
        return true
    }
    winnerVerify()
    playerchange()
    temporizador()
    return false
}
// Remove evento de click
function removeEventButtonClick() {
    for (let i = 0; i < columnButtons.length; i++) {
        columnButtons[i].removeEventListener('click', eventColumnSelect);
    }
}
// Cria evento de click 
const eventButtonClick = () => {
    for (let i = 0; i < columnButtons.length; i++) {
        columnButtons[i].addEventListener("click", criaDivAnima); //Willian => adicionei esse listener pra chamar a funcao que criara a div de animacao, essa funcao passara a chamar a funcao eventColumnSelect(). Mudanca aplicada para a animacao responder melhor

    }
};
eventButtonClick();

//function que e responsavel por criar a div que fara parte da animacao

function criaDivAnima(event) {
    eventColumnSelect(event);
    getClassValue()[1].style.visibility = "hidden"; //deixa todas as divs escondidas
    let divAnima = document.createElement("div");
    divAnima.className = "divAnimation";
    if (player == 1) { //insere a animacao dependendo do player que esta jogando
        divAnima.className = "divAnimation caixa2";
    } else {
        divAnima.className = "divAnimation caixa1";
    }

    //enquanto a coluna nao e preenchida execute TODO-01 senao, execute TODO-2

    //TODO-01
    selectedColumnButton.appendChild(divAnima);//coloca a div da animacao na coluna selecionada
    animationDisk();
    for (let i = 0; i < columnButtons.length; i++) { //condicional para nao evitar o evento de click enquanto a animacao nao termina
        columnButtons[i].removeEventListener("click", criaDivAnima);
    }

    setTimeout(function () { //funcao que tira a div de animacao quando chega na posicao correta
        if (selectedColumnButton.firstChild != null) {
            selectedColumnButton.removeChild(divAnima);
        }
        for (let i = 0; i < columnButtons.length; i++) {
            columnButtons[i].addEventListener("click", criaDivAnima);
            getClassValue()[1].style.visibility = "visible";//propriedade que mostra a div que foi "criada" ao fim da animacao
        }
    }, getClassValue()[0]);
    contarPlacar();
}


function getClassValue() {//funcao responsavel por pegar a posicao da div selecionada, linha e coluna.
    let classValue = document.getElementsByClassName("caixa");
    for (let index = 5; index >= 0; index--) {
        var a = (value) => classValue[Number(selectedColumnButton.id) + 7 * value];
        let newstr = a(index).className.replace(/\D/g, "");

        if (newstr !== "") {
            var b = index;
        }
    }

    switch (b) {//condicional que retorna o tempo da funcao que e responsavel por remover a div da animacao e fazer a parecer (visibility = "visible") da div que foi criada
        case 5:
            return [2200, a(b)];
            break;
        case 4:
            return [1500, a(b)];
            break;
        case 3:
            return [1150, a(b)];
            break;
        case 2:
            return [900, a(b)];
            break;
        case 1:
            return [700, a(b)];
            break;
        case 0:
            return [475, a(b)];
            break;
    }
}


// Reset
let recomecar = document.getElementById("recomecar")
recomecar.addEventListener('click', () => {
    //limpa a grade colocando somente 0

    //atualiza a grade
    setTimeout(() => {
        grade.map(x => x.fill(0))
        updateGrade()
    }, 2000)
    //reinicia o tempo
    setTimeout(() => {
        resetTimer()
    }, 4000)

    resetRec()
    //tira a div de vitoria para 
    //não dar conflito com a proxima
    winnerDiv.style.display = 'none'
    //começa a animação do recognizer
    for (let i = 0; i < columnButtons.length; i++) {
        columnButtons[i].style.pointerEvents = "all";
    }
})

let unidades = 0;
let minutos = 0;
let dezenas = "0"
let segundos = 0;
let start = 0;
let tempo = "0:00"
let stop = 0;
setInterval(() => unidades += 1, 1000)

// Temporizador
function temporizador() {
    setInterval(function () {
        if (segundos < 60) {
            if (unidades == 10 && dezenas == '0') {
                dezenas = parseInt(dezenas);
            }
            if (unidades == 10) {
                dezenas += 10;
                unidades = 0;
            }
            segundos = dezenas + unidades;
            if (segundos == 60) {
                dezenas = "0";
                unidades = 0;
                segundos = 0;
                minutos += 1;
            }
            tempo = minutos + ':' + segundos;
            if (stop == 0) {
                renderizaTimer(tempo);
            }
        }
    }, 1000)
    if (start == 0) {
        resetTimer();
        start = +1;
    }
}

//Cria a div do tempo
let divTimer = document.createElement("div");
divTimer.id = "timer";
document.getElementById("mainContainer").appendChild(divTimer);
divTimer.textContent = tempo;
function renderizaTimer(tempo) {
    divTimer.textContent = tempo;
}

//Reseta o timer
function resetTimer() {
    unidades = 0;
    minutos = 0;
    dezenas = "0"
    segundos = 0;
    tempo = "";
    stop = 0;
}

function stopTimer() {
    stop = 1;
}


//PLACAR
let count1 = 0;
let count2 = 0;
function contarPlacar() {
    let player1 = document.getElementById("player1");
    let player2 = document.getElementById("player2");


    if (playerWinner.offsetParent != null && player == 2) {
        count1++;
        player1.textContent = count1;
    } else if (playerWinner.offsetParent != null && player == 1) {
        count2++
        player2.textContent = count2;
    }
}




//Movimentação do robo para apagar os discos
let recognizerHead = document.getElementById('recognizerHead')
function resetRec() {
    recognizerHead.className = "resetRec"
    setTimeout(() => { recognizerHead.className = "" }, 4000)
}

//Inicio do jogo

let startScreen = document.getElementById('startScreen')

let titleGame = document.createElement('div')
titleGame.id = "titleGame"
startScreen.appendChild(titleGame)

let startButton = document.createElement('button')
startButton.id = "startButton"
startScreen.appendChild(startButton)

startButton.addEventListener('click', () => {
    mainContainer.removeChild(startScreen)

})



let startVideo = document.createElement('video')
startVideo.src = "./src/video/INTROLIGTRON.mp4"
startVideo.id = "startVideo"
startScreen.appendChild(startVideo)

let loopVideo = document.createElement('video')
loopVideo.src = "./src/video/GIFLIGTRON.mp4"
loopVideo.id = "startVideo"
loopVideo.loop = "true"

let play = 1
startScreen.addEventListener('click', () => {

    if (play === 1) {
        startVideo.play()
        startAudio.play()
        play++
    }
})

let startAudio = document.createElement('audio')
startAudio.src = "./src/music/tronMusic.mp3"
startAudio.loop = "true"
mainContainer.appendChild(startAudio)
startVideo.addEventListener('ended', () => {
    // startScreen.removeChild(startVideo)
    titleGame.className = "Appear"
    startButton.className = "Appear"

    setTimeout(() => {
        titleGame.style.display = "initial"
        startButton.style.display = "initial"
    }, 1500)
})


//   FUNCAO PARA ALTERAR O 100% (FINAL) DA ANIMACAO COM BASE NA POSICAO DE CADA DISCO.
//   PRECISAR PEGAR 
//   documen.getElementByClassname("caixa")[35], pega a primeira div da ultima coluna
//   document.getElementsByClassName("caixa")[41].getBoundingClientRect().top - pega o valor do top

function animationDisk() {
    let a = getClassValue()[1].getBoundingClientRect().top;
    let boxElement = document.getElementsByClassName("divAnimation");


    if (grade[0][selectedColumnButton.id] !== 0) {
        columnButtons[selectedColumnButton.id].style.pointerEvents = "none";
    }
    let animation = boxElement[0].animate([
        { transform: 'translate(0)' },
        { transform: 'translate(0px,' + a + 'px)' }
    ], getClassValue()[0]);
}