OS PANTERA TESTANDO CONFLITO

#Jogo LIG-4

###Busca e análise de requisitos - funções:

De cima para baixo, as mais importantes

Verificar se tem 6 discos na linha.


Se ganhou ou deu empate, o jogo para e informa o vencedor.
Verificar se ganhou - horizontal - vertical - diagonal.
// saber se ganhou, checar ganhador

Alternar cores no click. Exceto se tiver 6 discos na linha.
// caso clique e não altere nada, não passa vez
//passar a vez somente quando houver alteração no array

Exiba um disco preto ou vermelho.
//diferenciar os jogadores

Empilhe discos vermelhos e pretos em uma coluna usando um layout de caixa flex.
//flex colunm

Exiba um tabuleiro completo consistindo de 7 colunas.
// for que cria as divs, com mapeamento de um array


Configure uma função de handler de clique para cada coluna que adiciona um disco.
//click que adiciona o disco, relação ao jogador

Reveze os turnos! Mude a cor do próximo disco após um disco ser adicionado.
//alternar o jogadores 

Após encher uma coluna (6 discos), não permita que mais discos sejam adicionados.
//Caso não tenha mais zeros então é empate, isso não ocorre na logica aplicada



CSS




JAVASCRIPT



[
    [0,0,0,0,1,0,0,0]
    [0,0,0,0,0,1,0,0]
    [0,0,0,0,0,0,1,0]
    [0,0,0,0,0,0,0,1]
]